import com.smych.bragflag.country.Country;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Demo {

    public static void main(String[] args) {
        Country france = new Country("France", "Paris");
        france.visit();
        Country italy = new Country("Italy", "Rome");
        italy.visit();
        italy.visit();
        italy.visit();
        italy.visit();
        italy.visit();
        italy.visit();
        Country uk = new Country("Great Britain", "London");
        uk.visit();
        Country india = new Country("India", "New Deli");
        List<Country> countries = new ArrayList<>();
        countries.add(france);
        countries.add(italy);
        countries.add(uk);
        countries.add(india);

//        for (Country country : countries) {
//            if (country.visitCount() > 0)
//                System.out.println(country.name());
//        }
        countries.stream()
                .filter(country -> country.visitCount() > 0)
                .map(country -> country.name())
                .forEach(System.out::println);

//        List<String> capitals = new ArrayList<>();
//        for (Country country : countries) {
//            capitals.add(country.capital());
//        }
//        System.out.println(capitals.size());
        List<String> capitals = countries.stream()
                .map(country -> country.capital())
                .peek(System.out::println)
                .collect(Collectors.toList());
        System.out.println(capitals.size());
    }

}
