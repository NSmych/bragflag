package com.smych.bragflag.country;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/country")
public class CountryController {

    @GetMapping("/list")
    public List<String> countryList() {

        List<String> countries = new ArrayList<>();
        countries.add("Afghanistan");
        countries.add("Croatia");
        countries.add("Dominicans");
        countries.add("France");
        countries.add("Russia");
        countries.add("Zimbabwe");
        return countries;
    }

}
