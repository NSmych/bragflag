package com.smych.bragflag.country;

public class Country {

    private final String name;
    private final String capital;
    private int visitCount = 0;

    public Country(String name, String capital) {
        this.name = name;
        this.capital = capital;
    }

    public void visit() {
        visitCount++;
    }

    public String name() {
        return name;
    }

    public String capital() {
        return capital;
    }

    public int visitCount() {
        return visitCount;
    }
}
